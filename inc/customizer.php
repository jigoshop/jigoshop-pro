<?php

function tcx_register_theme_customizer( $wp_customize ) {
    $wp_customize->add_setting(
        'tcx_link_color',
        array(
            'default'     => '#000000'
        )
    );
    $wp_customize->add_setting(
        'tcx_bg_color',
        array(
            'default'     => '#000000'
        )
    );
    $wp_customize->add_setting(
        'tcx_bt_color',
        array(
            'default'     => '#000000'
        )
    );
 
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'link_color',
            array(
                'label'      => __( 'Link Color', 'tcx' ),
                'section'    => 'colors',
                'settings'   => 'tcx_link_color'
            )
        )
    );
     $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bg_color',
            array(
                'label'      => __( ' Header Nav background Color','tcx' ),
                'section'    => 'colors',
                'settings'   => 'tcx_bg_color'
            )
        )
    );
       $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'bt_color',
            array(
                'label'      => __( ' Button background Color','tcx' ),
                'section'    => 'colors',
                'settings'   => 'tcx_bt_color'
            )
        )
    );

     // LOGO
    $wp_customize->add_section( 'debut_logo_section' , array(
        'title'       => __( 'Logo', 'debut' ),
        'priority'    => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ) );
    $wp_customize->add_setting( 'debut_logo', array(
        'sanitize_callback' => 'esc_url_raw',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'debut_logo', array(
        'label'    => __( 'Logo', 'debut' ),
        'section'  => 'debut_logo_section',
        'settings' => 'debut_logo',
    ) ) );

        // BACKGROUND HEADER


    $wp_customize->add_section( 'header_img_section' , array(
        'title'       => __( 'Header background', 'debut' ),
        'priority'    => 30,
        'description' => 'Upload a background to replace the default ',
    ) );
    $wp_customize->add_setting( 'header_img', array(
        'sanitize_callback' => 'esc_url_raw',
    ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_img', array(
        'label'    => __( 'Header', 'bg_debut' ),
        'section'  => 'header_img_section',
        'settings' => 'header_img',
    ) ) );
}
add_action( 'customize_register', 'tcx_register_theme_customizer' ); 



