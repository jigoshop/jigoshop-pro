<?php get_header(); ?>

    <div id="content" class="site-content">
    <div class="ui container">
        <div class="ui stackable grid">
            <div class="col-sm-8">
                <?php do_action('jigoshop\shop\content\before'); ?>
                <?php echo $content; ?>
                <?php do_action('jigoshop\shop\content\after'); ?>
            </div>
            
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php
get_footer();

