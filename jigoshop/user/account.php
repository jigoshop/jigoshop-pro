<?php
use Jigoshop\Entity\Order\Status;
use Jigoshop\Helper\Order;
use Jigoshop\Helper\Product;
use Jigoshop\Helper\Render;

/**
 * @var $messages \Jigoshop\Core\Messages Messages container.
 * @var $content string Contents of cart page
 * @var $customer \Jigoshop\Entity\Customer The customer.
 * @var $editBillingAddressUrl string URL to billing address edition page.
 * @var $editShippingAddressUrl string URL to shipping address edition page.
 * @var $changePasswordUrl string URL to password changing page.
 * @var $myOrdersUrl string URL to My orders page.
 * @var $unpaidOrders \Jigoshop\Entity\Order[]
 * @var $downloadableItems \Jigoshop\Entity\Order\Item[]
 */
?>

<div class="js-title-page"><h1 class="js-title-head"><?php _e('My account', 'jigoshop-ecommerce'); ?></h1></div>
<?php Render::output('shop/messages', ['messages' => $messages]); ?>
<?= wpautop(wptexturize($content)); ?>
<div class="row clearfix">
    <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php _e('Billing address', 'jigoshop-ecommerce'); ?></h3>
                    <a title="Edit" href="<?= $editBillingAddressUrl; ?>" class="btn btn-xs btn-primary edit-btn pull-right "><?php _e('<i class="fas fa-edit"></i>', 'jigoshop-ecommerce'); ?></a>
                </div>
                <div class="panel-body clearfix">
                    <?php Render::output('user/account/address', ['address' => $customer->getBillingAddress()]); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php _e('Shipping address', 'jigoshop-ecommerce'); ?></h3>
                    <a title="Edit" href="<?= $editShippingAddressUrl; ?>" class="btn btn-xs btn-primary edit-btn pull-right"><?php _e('<i class="fas fa-edit"></i>', 'jigoshop-ecommerce'); ?></a>
                </div>
                <div class="panel-body">
                    <?php Render::output('user/account/address', ['address' => $customer->getShippingAddress()]); ?>
                </div>
            </div>
            <?php if(count($downloadableItems)): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php _e('Your Downloads', 'jigoshop-ecommerce'); ?></h3>
                    </div>
                    <ul class="list-group">
                        <?php foreach($downloadableItems as $data): ?>
                            <li class="list-group-item downloadable-item">
                                <a href="<?= Order::getItemDownloadLink($data['order'], $data['item']); ?>"><?= $data['item']->getName() ?></a>
                                <?php if($data['item']->getMeta('downloads')->getValue() < 0): ?>
                                    <span>
                                        <?= sprintf(__('Left: %d', 'jigoshop-ecommerce'), $data['item']->getMeta('downloads')->getValue()); ?>
                                    </span>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        <?php do_action('jigoshop\user\account\primary_panels', $customer); ?>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php _e('Account options', 'jigoshop-ecommerce'); ?></h3>
            </div>
            <ul class="list-group">
                <li class="list-group-item"><a href="<?= $changePasswordUrl; ?>"><?php _e('Change password', 'jigoshop-ecommerce'); ?></a></li>
                <li class="list-group-item"><a href="<?= $myOrdersUrl; ?>"><?php _e('My orders', 'jigoshop-ecommerce'); ?></a></li>
            </ul>
        </div>
        <div class="panel panel-warning" id="unpaid-orders">
            <div class="panel-heading">
                <h3 class="panel-title"><?php _e('Unpaid orders', 'jigoshop-ecommerce'); ?></h3>
            </div>
            <ul class="list-group">
                <?php foreach ($unpaidOrders as $order): ?>
                <li class="list-group-item clearfix">
                    <h4 class="list-group-item-heading" style="width: 100%;"><?= $order->getTitle(); ?></h4>
                    <dl class="dl-horizontal list-group-item-text">
                        <div class="line-holder">
                        <div class="col-sm-3 text-solid"><?php _e('Date', 'jigoshop-ecommerce'); ?></div>
                        <div  class="col-sm-9 text-right"><?= $order->getCreatedAt()->format(_x('d.m.Y, H:i', 'account', 'jigoshop-ecommerce')); ?></div>
                        </div>
                        <div class="line-holder">
                        <div class="col-sm-6 text-solid"><?php _e('Status', 'jigoshop-ecommerce'); ?></div>
                        <div class="col-sm-6 text-right"><?= Status::getName($order->getStatus()); ?></div>
                        </div>
                        <div class="line-holder">
                        <div  class="col-sm-6 text-solid"><?php _e('Total', 'jigoshop-ecommerce'); ?></div>
                        <div class="col-sm-6 text-right"><?= Product::formatPrice($order->getTotal()); ?></div>
                        </div>
                    </dl>
                    <a href="<?= Order::getPayLink($order); ?>" class="btn btn-success pull-right"><?php _e('Pay', 'jigoshop-ecommerce'); ?></a>
                </li>
                <?php endforeach; ?>
                <li class="list-group-item">
                    <a href="<?= $myOrdersUrl; ?>" class="btn btn-default"><?php _e('See more...', 'jigoshop-ecommerce'); ?></span></a>
                </li>
            </ul>
        </div>
        <?php do_action('jigoshop\user\account\secondary_panels', $customer); ?>
    </div>
    <?php do_action('jigoshop\user\account', $customer); ?>
</div>

