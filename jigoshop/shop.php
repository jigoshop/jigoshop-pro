<?php
use Jigoshop\Helper\Render;

/**
 * @var $content string Page contents.
 * @var $products array List of products to display.
 * @var $product_count int Number of all available products.
 * @var $messages \Jigoshop\Core\Messages Messages container.
 * @var $title string Title for the page.
 */
?>

<?php if(is_search()): ?>
	<div class="js-title-page"><h1 class="js-title-head"><?php _e('Search Results:', 'jigoshop-ecommerce'); ?> &ldquo;<?php the_search_query(); ?>&rdquo; <?php if (get_query_var('paged')) echo ' &mdash; Page '.get_query_var('paged'); ?></h1></div>
<?php else: ?>
	<?= apply_filters('jigoshop\shop\content\title', '<div class="js-title-page"><h1 class="js-title-head">'.$title.'</h1></div>', $title); ?>
<?php endif; ?>

<?php Render::output('shop/messages', ['messages' => $messages]); ?>

<?php if ($content): ?>
	<?= apply_filters('the_content', $content); ?>
<?php endif; ?>

<?php
Render::output('shop/list', [
	'products' => $products,
	'product_count' => $product_count,
]);
?>
