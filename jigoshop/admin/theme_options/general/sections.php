<?php
/**
 *
 */
$index = 0;
?>
<div class="form-group">
    <div style="width: 70%; float: left">
        <?php \Jigoshop\Admin\Helper\Forms::select([
            'placeholder' => __('Select section to add...', 'jigoshop-pro'),
            'name' => 'add-section-select',
            'id' => 'add-section-select',
            'options' => $available_sections,
            'value' => false,
        ]); ?>
    </div>
    <button type="button" class="btn btn-default pull-right" id="add-section"><?= __('Add', 'jigoshop-pro'); ?></button>
    <script>
        jQuery(document).ready(function() {
            var section_count = jQuery('#sections li').length;
            jQuery('#add-section').click(function() {
                section_count++
                var type = jQuery('#add-section-select').val();
                jQuery.ajax({
                    url: jigoshop.getAjaxUrl(),
                    type: 'post',
                    dataType: 'json',
                    cache: true,
                    data: {
                        action: 'jigoshop.admin.theme.add_section',
                        id: section_count,
                        type: type,
                    },
                }).done(function(result) {
                    if(result.success) {
                        jQuery('#sections').append(result.html);
                    }
                });
            });
            jQuery('#sections .remove-section').click(function(event) {
                jQuery(event.target).closest('li').remove();
            });
        });
    </script>
    <div class="clear"></div>
    <ul id="sections" class="list-group">
    <?php foreach($sections as $section) : $section['id'] = $index; $index++; ?>
        <?php if(isset($section['type']) && $section['type']) : ?>
            <?php \Jigoshop\Helper\Render::output('admin/theme_options/general/sections/'.$section['type'], $section); ?>
        <?php endif; ?>
    <?php endforeach; ?>
    </ul>
</div>
