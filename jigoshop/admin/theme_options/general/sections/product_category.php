<?php
/**
 *
 */
$terms = get_terms([
    'taxonomy' => 'product_category',
    'hide_empty' => true,
]);
$options = [];
foreach ($terms as $term) {
    $options[$term->term_id] = $term->name;
}
if(!isset($categories)) {
    $categories = [];
}
?>
<li class="list-group-item">
    <h4 class="list-group-item-heading clearfix">
        <span class="title"><?= __('Product category', 'jigoshop-pro'); ?></span>
        <button type="button" class="remove-section btn btn-default pull-right" title="<?php _e('Remove', 'jigoshop-ecommerce'); ?>"><span class="glyphicon glyphicon-remove"></span></button>
    </h4>
    <div class="list-group-item-text row clearfix">
        <input type="hidden" name="jigoshop[sections][<?= $id ?>][type]" value="<?= $type; ?>"/>
        <?php \Jigoshop\Admin\Helper\Forms::select([
            'name' => 'jigoshop[sections][' . $id . '][categories]',
            'label' => __('Categories','jigoshop-pro'),
            'multiple' => true,
            'options' => $options,
            'value' => $categories,
        ]); ?>
    </div>
</li>

