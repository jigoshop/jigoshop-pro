<?php
/**
 *
 */
?>
<li class="list-group-item">
    <h4 class="list-group-item-heading clearfix">
        <span class="title"><?= __('Blog', 'jigoshop-pro'); ?></span>
        <button type="button" class="remove-section btn btn-default pull-right" title="<?php _e('Remove', 'jigoshop-pro'); ?>"><span class="glyphicon glyphicon-remove"></span></button>
    </h4>
    <div class="list-group-item-text row clearfix">
        <input type="hidden" name="jigoshop[sections][<?= $id ?>][type]" value="<?= $type; ?>"/>
    </div>
</li>
