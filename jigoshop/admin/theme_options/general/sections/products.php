<?php
/**
 *
 */
?>
<li class="list-group-item">
    <h4 class="list-group-item-heading clearfix">
        <span class="title"><?= __('Products', 'jigoshop-pro'); ?></span>
        <button type="button" class="remove-section btn btn-default pull-right" title="<?php _e('Remove', 'jigoshop-ecommerce'); ?>"><span class="glyphicon glyphicon-remove"></span></button>
    </h4>
    <div class="list-group-item-text row clearfix">
        <input type="hidden" name="jigoshop[sections][<?= $id ?>][type]" value="<?= $type; ?>"/>
        <?php \Jigoshop\Admin\Helper\Forms::select([
            'name' => 'jigoshop[sections][' . $id . '][tabs]',
            'label' => __('Tabs','jigoshop-pro'),
            'multiple' => true,
            'options' => [
                'random' => __('Random', 'jigoshop-pro'),
                'sale' => __('Sale', 'jigoshop-pro'),
                'new' => __('New', 'jigoshop-pro'),
                'featured' => __('Featured', 'jigoshop-pro'),
                'best_sellers' => __('Best sellers', 'jigoshop-pro'),
            ],
            'value' => $tabs,
        ]); ?>
    </div>
</li>
