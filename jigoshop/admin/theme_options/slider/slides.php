<?php
use Jigoshop\Admin\Helper\Forms;
use Jigoshop\Helper\Render;
?>

<table class="table table-striped" id="slides">
	<thead>
		<tr>
			<th><?php echo __('File', 'jigoshop-pro'); ?></th>
			<th><?php echo __('Text', 'jigoshop-pro'); ?></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($slides as $slideId => $slide) {
			Render::output('admin/theme_options/slider/slide', [
				'slideId' => $slideId,
				'slide' => $slide
			]);
		}
		?>
	</tbody>
</table>

<button class="btn btn-default" id="add-slide">
	<span class="glyphicon glyphicon-plus"></span>
	<?php echo __('Add slide', 'jigoshop-pro'); ?>
</button>

<div id="slide-edit-form" style="display: none">
	<input type="hidden" id="slide-id" name="jigoshop[slideId]" value="" />

	<?php
	Forms::userDefined([
		'name' => 'selectImage',
		'label' => __('Image', 'jigoshop-pro'),
		'display' => function($field) {
			return Render::get('admin/theme_options/slider/select_image', []);
		}
	]);

	Forms::text([
		'id' => 'slide-text',
		'name' => 'jigoshop[text]',
		'label' => __('Text', 'jigoshop-pro')
	]);
	?>
</div>

<script>
	jQuery(function() {
		jQuery('#select-image').click(launchWpMedia);
		jQuery('#add-slide').click(addSlide);
		jQuery('.edit-slide').click(editSlide);
		jQuery('.remove-slide').click(removeSlide);

		function showForm(slideId) {
			jQuery('#slide-id').val(slideId);

			jQuery('#slide-image-id').val(0);
			jQuery('#slide-image-name').text('');
			jQuery('#slide-text').text('');

			if(slideId !== '') {
				jQuery('#slides').find('tbody').find('tr').each(function(index, element) {
					if(jQuery(element).data('slide-id') != slideId) {
						return;
					}

					jQuery('#selected-image-id').val(jQuery(element).data('slide-image-id'));
					jQuery('#selected-image-name').text(jQuery(element).find('.slide-image-name').text());
					jQuery('#slide-text').val(jQuery(element).find('.slide-text').text());
				});
			}

			if(!jQuery('#slide-edit-form').is(':visible')) {
				jQuery('#slide-edit-form').slideToggle();
			}
		}

		function launchWpMedia(e) {
			e.preventDefault();

			var wpMedia = wp.media({
				multiple: false,
				library: {
					type: 'image'
				}
			}).on('close', function(e) {
				var image = wpMedia.state().get('selection').first();

				if(image === undefined) {
					$('#selected-image-id').val(0);
					$('#selected-image-name').text('');

					return;
				}

				jQuery('#selected-image-id').val(image.attributes.id);
				jQuery('#selected-image-name').text(image.attributes.filename);
			}).open();
		}

		function addSlide(e) {
			e.preventDefault();

			showForm('');
		}

		function editSlide(e) {
			e.preventDefault();

			showForm(jQuery(e.delegateTarget).parents('tr').data('slide-id'));
		}

		function removeSlide(e) {
			e.preventDefault();

			var slideId = jQuery(e.delegateTarget).parents('tr').data('slide-id');

			jQuery.post(ajaxurl, {
				action: 'jigoshop_pro_removeSlide',
				slideId: slideId
			}, function() {
				location.href = document.URL;
			});
		}
	});
</script>