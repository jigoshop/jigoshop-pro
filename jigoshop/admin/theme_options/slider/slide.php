<tr data-slide-id="<?php echo $slideId; ?>" data-slide-image-id="<?php echo $slide['imageId']; ?>">
	<td class="slide-image-name">
		<?php 
		$meta = wp_get_attachment_metadata($slide['imageId']);
		if($meta !== false) {
			echo basename($meta['file']);
		}
		else {
			echo '-';
		}
		?>
	</td>
	<td class="slide-text"><?php echo $slide['text']; ?></td>
	<td>
		<a href="" class="btn btn-default edit-slide">
			<span class="glyphicon glyphicon-plus"></span>

			<?php echo __('Edit', 'jigoshop-pro'); ?>
		</a>

		<a href="" class="btn btn-default remove-slide">
			<span class="glyphicon glyphicon-remove"></span>
		</a>
	</td>
</tr>