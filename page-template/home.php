<?php
/*
Template Name: Jigoshop Pro
*/
$generalOptions = \ThemeOptions\Init::getTabOptions('general');
$sliderOptions = \ThemeOptions\Init::getTabOptions('slider');
?>
<?php get_header('contact'); ?>

<!-- Carousle & Img -->

<div class="js-main-container">
<div class="js-row js-row-fluid js-box-top js-row-has-fill">
<div class="js-row-container">
	<div class="js-column-container col-sm-12">
		<div class="js-column-inner">
    	<div class="js-wrapper">
    		
    			<div class="js-row js-inner js-row-fluid js-silder-static">
    				<div class="js-home-slider js-layout-2 js-column js-column-container big-left-img col-sm-8">
    					<div class="js-inner-box">
    						<div class="js-wrapper">
    							<div class="inner-box big-screen jigo-slideshow-outer-container">

                  <!-- Slideshow container -->

                      <div class="jigo-slideshow-container">

                         <!-- Full-width images with number and caption text -->

                            <?php 
                            foreach($sliderOptions['slides'] as $slideId => $slide) {
                              $slideUrl = '';
                              $slideImage = wp_get_attachment_image_src($slide['imageId'], 'full');
                              if($slideImage !== false && isset($slideImage[0])) {
                                $slideUrl = $slideImage[0];
                              }                              
                              ?>
                              <div class="jigoSlide fades">
                                <div class="numbertext"><?php echo $slideId + 1; ?> / <?php echo count($sliderOptions['slides']); ?></div>
                                <?php 
                                if($slideUrl) {
                                  ?>
                                  <img src="<?php echo $slideUrl; ?>" />
                                  <?php
                                }
                                ?>
                                <div class="text"><?php echo $slide['text']; ?></div>
                              </div>
                              <?php
                            }
                            ?>

                                <!-- Next and previous buttons -->

                            <a class="prevSlide">&#10094</a>
                            <a class="nextSlide">&#10095;</a>
                      </div>
                      <br>
                        <div style="text-align:center">
                          <?php 
                          foreach($sliderOptions['slides'] as $slide) {
                            ?>
                            <span class="slideDot dot"></span>
                            <?php
                          }
                          ?>
                        </div>

				    					<!-- <img class="border-item" src="http://d.jigo.pl:10003/wp-content/uploads/2018/03/img_fjords.jpg"> -->
				    					<!-- <div class="js-text"> some text </div> -->
			    					
			    				</div>
    						</div>
    					</div>
    				</div>
    				<div class=" js-home-2-static js-layout-2 js-column js-column-container right-container col-sm-4">
    					<div class="js-column-inner">
    						<div class="js-wrapper">
			    				<div class="js-text-column js-content-element js-box js-box-1 small-top-right-img">
			    					<div class="inner-box">
			    					<img class="border-item" src="http://d.jigo.pl:10003/wp-content/uploads/2018/03/img_fjords.jpg">
			    					<div class="js-text"> 
			    						<h2>2018 sale</h2>
										<h5>get up to</h5>
										<h4>20% sale</h4> 
			    					 </div>
			    					</div>
			    				</div>
			    				<div class="js-text-column js-content-element js-box js-box-2 small-bot-right-img">
			    					<div class="inner-box">
			    					<img class="border-item" src="http://d.jigo.pl:10003/wp-content/uploads/2018/03/img_fjords.jpg">
			    					<div class="js-text"> 
			    					<h2>15% off</h2>
										<p>On trending product models</p>
				    					
									</div>
			    					</div>
			    				</div>
			    			</div>
    					</div>
    				</div>
    			</div>
    			<div class="js-row js-inner js-row-fluid js-our-service js-layout-2">
    			<div class="js-box js-box-1 js-column js-column-container col-sm-4">
    			<div class="js-column-inner">
    				<div class="js-wrapper">
    					<div class="js-text-column js-content-element js-box-inner">
    						<div class="icon-box"><i class="fas fa-truck-moving fa-fw"></i>

</div>
    						<div class="js-text-box">
    							<h4>Free Shipping</h4>
								<p>Free shipping on all orders over $100</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			</div>
    			<div class="js-box js-box-2 js-column js-column-container col-sm-4">
    			<div class="js-column-inner">
    				<div class="js-wrapper">
    					<div class="js-text-column js-content-element js-box-inner">
    						<p class="icon-box"><i class="fas fa-gem fa-fw"></i></p>
    						<div class="js-text-box">
    							<h4>Money back</h4>
								<p>100% money back guarantee</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			</div>
    			<div class="js-box js-box-3 js-column js-column-container col-sm-4">
    			<div class="js-column-inner">
    				<div class="js-wrapper">
    					<div class="js-text-column js-content-element js-box-inner">
    						<p class="icon-box"><i class="fas fa-life-ring fa-fw"></i></p>
    						<div class="js-text-box">
    							<h4>Online Support</h4>
								<p>Service support fast 24/7</p>
    						</div>
    					</div>
    				</div>
    			</div>
    			</div>
    		</div>
    	</div>
    	</div>
    	</div>
    	</div>
    	</div>
<?php $index = 0; foreach ($generalOptions['sections'] as $section) : ?>
    <?php if(isset($section['type']) && in_array($section['type'], ['blog', 'products', 'product_category', 'product_category_banner'])): ?>
        <?php if($section['type'] == 'blog'): ?>
            <?php include __DIR__ . '/home/blog_section.php'; ?>
        <?php elseif ($section['type'] == 'products'): ?>
            <?php include __DIR__ . '/home/products_section.php'; ?>
            <?php $index += count($section['tabs']); ?>
        <?php elseif ($section['type'] == 'product_category'): ?>
            <?php include __DIR__ . '/home/product_category_section.php'; ?>
        <?php elseif ($section['type'] == 'product_category_banner'): ?>
            <?php include __DIR__ . '/home/product_category_banner_section.php'; ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>
<!-- FOOTER CLASS START HERE -->

<!-- LOGOS carousel -->

<!--  BANNER -->

    
</div>
<?php get_footer(); ?>
<script type="text/javascript">
  function openTabs1(evt, tabName) {
    var $container = jQuery(evt.target).closest('.js-row');
    jQuery('.tabcontent', $container).hide();
    jQuery('#'+ tabName).show();
  }
  jQuery('.js-row').each(function ($i, $element) {
    jQuery('.tabcontent', $element).first().show();
  });


</script>
<script>
  var sliderActiveSlide = 0;
  var sliderInterval = 0;

  jQuery(function() {
    jQuery('.slideDot').click(sliderSwitchSlideByDot);
    jQuery('.prevSlide, .nextSlide').click(sliderSwitchSlideByArrow);
    sliderInterval = setInterval(sliderIntervalTick, 5000);

    jQuery('.jigo-slideshow-outer-container').mouseenter(function() {
      clearInterval(sliderInterval);

      sliderInterval = 0;
    });

    jQuery('.jigo-slideshow-outer-container').mouseleave(function() {
      if(sliderInterval === 0) {
        sliderInterval = setInterval(sliderIntervalTick, 5000);
      }
    });

    sliderSwitchSlide(sliderActiveSlide);
  });

  function sliderSwitchSlideByDot(e) {
    e.preventDefault();

    var slideId = jQuery('.slideDot').index(e.delegateTarget);

    if(slideId != sliderActiveSlide) {
      sliderSwitchSlide(slideId);
    }
  }

  function sliderSwitchSlideByArrow(e) {
    var slideId = sliderActiveSlide;

    if(jQuery(e.delegateTarget).hasClass('prevSlide')) {
      if(sliderActiveSlide > 0) {
        slideId--;
      }
    }
    else if(jQuery(e.delegateTarget).hasClass('nextSlide')) {
      if(sliderActiveSlide < (jQuery('.jigoSlide').length - 1)) {
        slideId++;
      }
    }

    if(sliderActiveSlide != slideId) {
      sliderSwitchSlide(slideId);
    }
  }

  function sliderIntervalTick() {
    var slideId = sliderActiveSlide;

    slideId++;

    if(slideId > (jQuery('.jigoSlide').length - 1)) {
      slideId = 0;
    }

    sliderSwitchSlide(slideId);
  }

  function sliderSwitchSlide(slideId) {
    sliderActiveSlide = slideId;

    jQuery('.jigoSlide').each(function(index, element) {
      if(index == slideId) {
        jQuery(element).show();
      }
      else {
        jQuery(element).hide();
      }
    });

    jQuery('.slideDot').each(function(index, element) {
      if(index == slideId) {
        jQuery(element).addClass('active');
      }
      else {
        jQuery(element).removeClass('active');
      }
    });
  }
</script>