<?php
/**
 *
 */
?>
<div class="js-row js-row-fluid js-latest-post">
    <div class="js-row-container">
        <div class="js-column js-column-container col-sm-12">
            <div class="js-column-inner">
                <div class="js-wrapper">
                    <div class="js-text-column js-content-element js-title1">
                        <div class="js-wrapper">
                            <h3>
                                <span><?= __('Blog post', 'jigoshop-pro'); ?></span>
                            </h3>
                        </div>
                    </div>
                    <div class="js-post-corousel jigo-carousel jigo-theme" data-col="4" style="opacity:1;display: block;">
                        <div id="post-wrap" class="full-width-wrap">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="post-single">
                                    <div class="post-title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a>
                                        <span class="post-time"><?php the_time('F j, Y'); ?></span>
                                        <?php if(get_the_author() != "adam"): ?>, by <span class="post-author"><?php the_author_posts_link() ?></span><?php endif; ?>
                                        </span>
                                    </div>
                                </div><!--.post-single-->
                            <?php endwhile; else: ?>
                                <div class="no-results">
                                    <p><strong>There has been an error.</strong></p>
                                    <p>We apologize for any inconvenience, please <a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>">return to the home page</a> or use the search form below.</p>
                                    <?php get_search_form(); /* outputs the default Wordpress search form */ ?>
                                </div><!--noResults-->
                            <?php endif; ?>

                            <div class="alignleft"><?php next_posts_link('&laquo; Previous Entries') ?></div>
                            <div class="alignright"><?php previous_posts_link('Next Entries &raquo;') ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

