<?php
/**
 *
 */
?>
<div class="js-row js-row-fluid js-home-tabs">
    <div class="js-title-container">
        <ul class="js-home-tabs-title">
            <?php for($i = 0; $i < count($section['tabs']); $i++) : ?>
                <?php if($section['tabs'][$i] == 'random'): $title = __('Random', 'jigoshop-pro'); ?>
                <?php elseif ($section['tabs'][$i] == 'sale'): $title = __('On Sale', 'jigoshop-pro');  ?>
                <?php elseif ($section['tabs'][$i] == 'new'): $title = __('New', 'jigoshop-pro');  ?>
                <?php elseif ($section['tabs'][$i] == 'featured'): $title = __('Featured', 'jigoshop-pro');  ?>
                <?php elseif ($section['tabs'][$i] == 'best_sellers'): $title = __('Best Sellers', 'jigoshop-pro');  ?>
                <?php endif; ?>
                <li class="<?= $index+$i ?>-tab">
                    <a class="js-tab-link active" onclick="openTabs1(event, 'js-content-products-<?= $index+$i; ?>')"  rel="js-content-products-<?= $index+$i; ?>">
                        <?= $title; ?>
                    </a>
                </li>
            <?php endfor; ?>
        </ul>
        <?php for($i = 0; $i < count($section['tabs']); $i++) : ?>
            <?php if($section['tabs'][$i] == 'random'):
                $args = [
                    'posts_per_page' => 4,
                    'post_type' => Jigoshop\Core\Types::PRODUCT,
                    'post_status' => 'publish',
                    'orderby' => 'rand',
                    'meta_query' => [
                        [
                            'key' => 'visibility',
                            'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                            'compare' => 'IN',
                        ],
                    ]
                ];
            elseif ($section['tabs'][$i] == 'sale'):
                $args = [
                    'posts_per_page' => 4,
                    'post_type' => Jigoshop\Core\Types::PRODUCT,
                    'post_status' => 'publish',
                    'orderby' => 'rand',
                    'meta_query' => [
                        [
                            'key' => 'visibility',
                            'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                            'compare' => 'IN',
                        ],
                        [
                            'key' => 'sales_enabled',
                            'value' => 1,
                        ],
                    ]
                ];
            elseif ($section['tabs'][$i] == 'new'):
                $args = [
                    'posts_per_page' => 4,
                    'post_type' => Jigoshop\Core\Types::PRODUCT,
                    'post_status' => 'publish',
                    'orderby' => 'date',
                    'order' => 'desc',
                    'nopaging' => false,
                    'meta_query' => [
                        [
                            'key' => 'visibility',
                            'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                            'compare' => 'IN',
                        ],
                    ]
                ];
            elseif ($section['tabs'][$i] == 'featured'):
                $args = [
                    'posts_per_page' => 4,
                    'post_type' => Jigoshop\Core\Types::PRODUCT,
                    'post_status' => 'publish',
                    'meta_key' => 'featured',
                    'meta_value' => '1',
                    'meta_query' => [
                        [
                            'key' => 'visibility',
                            'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                            'compare' => 'IN',
                        ],
                    ]
                ];
            elseif ($section['tabs'][$i] == 'best_sellers'):
                $args = [
                    'posts_per_page' => 4,
                    'post_type' => Jigoshop\Core\Types::PRODUCT,
                    'post_status' => 'publish',
                    'meta_key' => 'stock_sold',
                    'orderby' => 'meta_value_num+0',
                    'order' => 'desc',
                    'nopaging' => false,
                    'meta_query' => [
                        [
                            'key' => 'visibility',
                            'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                            'compare' => 'IN',
                        ],
                    ]
                ];

            endif;
            $products = \Jigoshop\Integration::getProductService()->findByQuery(new WP_Query($args));
            ?>
            <div id="js-content-products-<?= $index+$i; ?>" class="tabcontent">
                <?php foreach($products as $product): ?>
                    <?php printf('<div class="col-sm-3 newItem"><a href="%s"><div class="centerAlign shopItem">%s <div class="itemInfo"><div class="bottomAlign"><a class="linkJigo" href="%s">%s</a><div class="priceJigo">%s</div></div></div></div></a></div>', $product->getLink(),\Jigoshop\Helper\Product::getFeaturedImage($product), $product->getLink(), $product->getName(), \Jigoshop\Helper\Product::getPriceHtml($product)); ?>
                <?php endforeach; ?>
            </div>
        <?php endfor; ?>
    </div>
</div>
