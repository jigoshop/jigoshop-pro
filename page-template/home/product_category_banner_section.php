<?php
/**
 *
 */
$term = get_term_by('id', (int)$section['category'], 'product_category');
if($term instanceof \WP_Term) :
    $args = [
        'posts_per_page' => 4,
        'post_type' => Jigoshop\Core\Types::PRODUCT,
        'post_status' => 'publish',
        'orderby' => 'rand',
        'nopaging' => false,
        'meta_query' => [
            [
                'key' => 'visibility',
                'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                'compare' => 'IN',
            ],
        ],
        'tax_query' => [
            [
                'taxonomy' => 'product_category',
                'field' => 'term_id',
                'terms' => $term->term_id
            ]
        ],
    ];
    $products = \Jigoshop\Integration::getProductService()->findByQuery(new WP_Query($args));
    $from = 9999999999;
    if(count($products)) {
        foreach($products as $product) {
            if($product instanceof \Jigoshop\Entity\Product\Purchasable) {
                if($product->getPrice() < $from) {
                    $from = $product->getPrice();
                }
            } elseif ($product instanceof \Jigoshop\Entity\Product\Variable) {
                if($product->getLowestPrice() < $from) {
                    $from = $product->getLowestPrice();
                }
            }
        }
    }
    $image = \Jigoshop\Helper\ProductCategory::getImage($term->term_id);
    ?>
    <div class="js-row js-row-fluid js-home-2-static-2 js-row-has-fill">
        <div class="js-row-container">
            <div class="js-column js-column-container col-sm-5">
                <div class="js-column-inner">
                    <div class="js-wrapper">
                        <div class="js-single-image js-content-element js-align-left js-box-image">
                            <figure class="js-wrapper js-figure">
                                <div class="js-single-image-wrapper js-box-border-gray">
                                    <img src="<?= $image['image']; ?>">
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="js-column js-column-container col-sm-7">
                <div class="js-column-inner">
                    <div class="js-wrapper">
                        <div class="js-text-column js-content-element js-box-text">
                            <div class="js-wrapper">
                                <div class="js-price">
                                    <?= __('From:', 'jigoshop-pro'); ?>
                                    <strong><?= \Jigoshop\Helper\Product::formatPrice($from); ?></strong>
                                </div>
                                <h1><?= $term->name; ?></h1>
                                <p><a class="js-view" href="<?= get_term_link($term); ?>"><?= __('view collection', 'jigoshop-pro'); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
