<?php
/**
 *
 */
?>
<div class="js-row js-row-fluid js-home-tabs">
    <div class="js-title-container">
        <ul class="js-home-tabs-title">
            <?php foreach($section['categories'] as $category) : ?>
                <?php $term = get_term_by('id', (int)$category, 'product_category'); ?>
                <?php if($term instanceof \WP_Term): ?>
                    <li class="<?= $category; ?>-tab">
                        <a class="js-tab-link active" onclick="openTabs1(event, 'js-content-category-<?= $category; ?>')"  rel="js-content-category-<?= $category; ?>">
                            <?= $term->name; ?>
                        </a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <?php foreach($section['categories'] as $category) : ?>
            <?php $args = [
                'posts_per_page' => 4,
                'post_type' => Jigoshop\Core\Types::PRODUCT,
                'post_status' => 'publish',
                'orderby' => 'rand',
                'nopaging' => false,
                'meta_query' => [
                    [
                        'key' => 'visibility',
                        'value' => [Jigoshop\Entity\Product::VISIBILITY_CATALOG, Jigoshop\Entity\Product::VISIBILITY_PUBLIC],
                        'compare' => 'IN',
                    ],
                ],
                'tax_query' => [
                    [
                        'taxonomy' => 'product_category',
                        'field' => 'term_id',
                        'terms' => $category
                    ]
                ],
            ];
            $products = \Jigoshop\Integration::getProductService()->findByQuery(new WP_Query($args));
            ?>
            <div id="js-content-category-<?= $category; ?>" class="tabcontent">
                <?php foreach($products as $product): ?>
                    <?php printf('<div class="col-sm-3 newItem"><a href="%s"><div class="centerAlign shopItem">%s <div class="itemInfo"><div class="bottomAlign"><a class="linkJigo" href="%s">%s</a><div class="priceJigo">%s</div></div></div></div></a></div>', $product->getLink(),\Jigoshop\Helper\Product::getFeaturedImage($product), $product->getLink(), $product->getName(), \Jigoshop\Helper\Product::getPriceHtml($product)); ?>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

