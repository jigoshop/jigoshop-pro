<?php
// Initialize ThemeOptions.
require_once 'ThemeOptions/Init.php';

function tcx_customizer_css() {
    ?>
    <style type="text/css">
        a { color: <?php echo get_theme_mod( 'tcx_link_color' ); ?>; }
        .blog-masthead { background:  <?php echo get_theme_mod( 'tcx_bg_color' ); ?>;}
        .header-image { background:  url(<?php echo get_theme_mod( 'header_img' ); ?>);}
    </style>
    <?php
}
add_action( 'wp_head', 'tcx_customizer_css' );

require_once get_template_directory() . '/inc/customizer.php';
add_theme_support( 'post-thumbnails' );


// custom menus

function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
  )
);
}
add_action( 'init', 'wpb_custom_new_menu' );
add_action('wp_ajax_jigoshop.admin.theme.add_section', function() {
    if(isset($_POST['id'], $_POST['type']) && in_array($_POST['type'], ['blog', 'products', 'product_category', 'product_category_banner'])) {
        $html = \Jigoshop\Helper\Render::get('admin/theme_options/general/sections/'. $_POST['type'], [
            'id' => $_POST['id'],
            'type' => $_POST['type'],
        ]);

        echo json_encode([
            'success' => true,
            'html' => $html,
        ]);
    }
    exit;
});