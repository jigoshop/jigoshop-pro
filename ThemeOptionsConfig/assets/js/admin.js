jQuery(function() {
	jQuery('#section-add-button').click(addSection);

	bindSectionControls();

	jQuery('#slider-add-button').click(addSlide);

	bindSlideControls();
});

function bindSectionControls() {
	jQuery('.section-remove-button').unbind('click').click(removeSection);
}

function addSection(e) {
	var type, lastSectionId;

	e.preventDefault();

	type = jQuery('#section').val();
	if(jQuery('#sections li.list-group-item').length) {
		lastSectionId = jQuery('#sections li.list-group-item').last().data('section-id');
	}
	else {
		lastSectionId = -1;
	}

	jQuery.post(ajaxurl, {
		action: 'admin.jigoshop-pro.addSection',
		type: type,
		lastSectionId: lastSectionId
	}, function(data) {
		if(data.success) {
			jQuery('#sections').append(data.section);

			bindSectionControls();
		}
	}, 'json');
}

function removeSection(e) {
	e.preventDefault();

	jQuery(e.delegateTarget).parents('li').remove();
}

function bindSlideControls() {
	jQuery('.slide-select-image-button').unbind('click').click(selectSlideImage);
	jQuery('.slide-remove-button').unbind('click').click(removeSlide);
}

function addSlide(e) {
	var lastSlideId;

	e.preventDefault();

	if(jQuery('#slides li.list-group-item').length) {
		lastSectionId = jQuery('#slides li.list-group-item').last().data('slide-id');
	}
	else {
		lastSectionId = -1;
	}	

	jQuery.post(ajaxurl, {
		action: 'admin.jigoshop-pro.addSlide',
		lastSectionId: lastSlideId		
	}, function(data) {
		if(data.success) {
			jQuery('#slides').append(data.slide);

			bindSlideControls();
		}
	}, 'json');
}

function selectSlideImage(e) {
	var wpMedia, slide, image;

	e.preventDefault();

	slide = jQuery(e.delegateTarget).parents('li');

    wpMedia = wp.media({
        multiple: false,
        library: {
                type: 'image'
        }
    }).on('close', function(e) {
	    image = wpMedia.state().get('selection').first();

	    if(image === undefined) {
            jQuery(slide).find('.slide-image-id').val(0);
            jQuery(slide).find('.slide-image-name').text('');

            return;
	    }

	    jQuery(slide).find('.slide-image-id').val(image.attributes.id);
	    jQuery(slide).find('.slide-image-name').text(image.attributes.filename);
    }).open();	
}

function removeSlide(e) {
	e.preventDefault();

	jQuery(e.delegateTarget).parents('li').remove();
}