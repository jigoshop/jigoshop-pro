<?php
namespace ThemeOptionsConfig;

use ThemeOptions\Helper\Scripts;
use ThemeOptions\Init;
use ThemeOptions\ThemeInterface;

class Theme implements ThemeInterface {
	const SLUG = 'jigoshop_theme_jigoshop_pro';
	const NAME = 'Jigoshop Pro';

	private $tabs;

	public function __construct() {
		$this->tabs = [
			new \ThemeOptionsConfig\Tab\General(),
			new \ThemeOptionsConfig\Tab\Slider()
		];

		add_action('admin_enqueue_scripts', function() {
			Scripts::add(sprintf('%s.admin', self::SLUG), sprintf('%s/ThemeOptionsConfig/assets/js/admin.js', Init::getThemeURL()), ['jquery']);
		});
	}

	public function getSlug() {
		return self::SLUG;
	}

	public function getName() {
		return self::NAME;
	}

	public function getTabs() {
		return $this->tabs;
	}
}

return new Theme();