<?php
namespace ThemeOptionsConfig\Tab;

use ThemeOptions\Helper\Render;
use ThemeOptions\Tab\ThemeTabInterface;

class Slider implements ThemeTabInterface {
	const SLUG = 'slider';
	const TITLE = 'Slider';

	private $options;

	public function __construct() {
		add_action('wp_ajax_admin.jigoshop-pro.addSlide', [$this, 'ajaxAddSlide']);
	}

	public function getSlug() {
		return self::SLUG;
	}

	public function getTitle() {
		return self::TITLE;
	}

	public function getSections() {
		return [
			[
				'id' => 'slider',
				'title' => __('Slider', 'jigoshop-pro'),
				'display' => [$this, 'renderSliderSection']
			]
		];
	}

	public function renderSliderSection() {
		return Render::get('admin/theme_options/slider/section', [
			'slides' => $this->options['slides']
		]);
	}

	public function ajaxAddSlide() {
		$slideId = $_POST['lastSlideId'] + 1;

		echo json_encode([
			'success' => true,
			'slide' => Render::get('admin/theme_options/slider/slide', [
				'id' => $slideId
			])
		]);

		exit;
	}

	public function getDefaultOptions() {
		return [
			'slides' => []
		];
	}

	public function setOptions($options) {
		$this->options = $options;
	}

	public function validate($options) {
		$slides = [];
		foreach($options['slides'] as $slide) {
			if(!isset($slide['imageId'])) {
				continue;
			}

			$slides[] = $slide;
		}
		$options['slides'] = $slides;

		return $options;
	}
}