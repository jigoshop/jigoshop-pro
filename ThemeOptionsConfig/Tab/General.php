<?php
namespace ThemeOptionsConfig\Tab;

use ThemeOptions\Helper\Render;
use ThemeOptions\Tab\ThemeTabInterface;

class General implements ThemeTabInterface {
	const SLUG = 'general';
	const TITLE = 'General';

	private $options;

	private static $sections;
	private static $productOptions;

	public function __construct() {
		self::$sections = [
			'blog' => __('Blog', 'jigoshop-pro'),
			'products' => __('Products', 'jigoshop-pro'),
			'product_category' => __('Product category', 'jigoshop-pro'),
			'product_category_banner' => __('Product category banner', 'jigoshop-pro')
		];

		self::$productOptions = [
			'random' => __('Random', 'jigoshop-pro'),
			'new' => __('New', 'jigoshop-pro'),
			'sale' => __('Sale', 'jigoshop-pro'),
			'featured' => __('Featured', 'jigoshop-pro'),
			'best-sellers' => __('Best sellers', 'jigoshop-pro')
		];

		add_action('wp_ajax_admin.jigoshop-pro.addSection', [$this, 'ajaxAddSection']);
	}

	public function getSlug() {
		return self::SLUG;
	}

	public function getTitle() {
		return self::TITLE;
	}

	public function getSections() {
		return [
			[
				'id' => 'general',
				'title' => __('General', 'jigoshop-pro'),
				'display' => [$this, 'renderGeneralSection']
			]
		];
	}

	public function renderGeneralSection() {
		$terms = get_terms([
			'taxonomy' => 'product_category',
			'hide_empty' => false
		]);
		$productCategories = [];
		foreach($terms as $term) {
			$productCategories[$term->term_id] = $term->name;
		}

		return Render::get('admin/theme_options/general/section', [
			'sectionOptions' => self::$sections,
			'sections' => $this->options['sections'],
			'productOptions' => self::$productOptions,
			'productCategories' => $productCategories
		]);
	}

	public function ajaxAddSection() {
		$sectionId = $_POST['lastSectionId'] + 1;

		if(!isset(self::$sections[$_POST['type']])) {
			echo json_encode([
				'success' => false,
				'error' => __('Invalid section type specified.', 'jigoshop-pro')
			]);

			exit;
		}

		$terms = get_terms([
			'taxonomy' => 'product_category',
			'hide_empty' => false
		]);
		$productCategories = [];
		foreach($terms as $term) {
			$productCategories[$term->term_id] = $term->name;
		}

		$section = Render::get(sprintf('admin/theme_options/general/%s', $_POST['type']), [
			'id' => $sectionId,
			'productOptions' => self::$productOptions,
			'productCategories' => $productCategories
		]);

		echo json_encode([
			'success' => true,
			'section' => $section
		]);	

		exit;	
	}

	public function getDefaultOptions() {
		return [
			'sections' => []
		];
	}

	public function setOptions($options) {
		$this->options = $options;
	}

	public function validate($options) {
		unset($options['section']);

		if(!isset($options['sections']) || !is_array($options['sections'])) {
			$options['sections'] = [];
		}

		$sections = [];
		foreach($options['sections'] as $sectionId => $section) {
			if(!isset($section['type'])) {
				continue;
			}

			$sections[] = $section;
		}
		$options['sections'] = $sections;

		return $options;
	}
}