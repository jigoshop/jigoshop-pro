<?php
use ThemeOptions\Helper\Render;
?>

<div class="col-sm-12">
	<a href="#" class="btn btn-primary" id="slider-add-button">
		<span class="glyphicon glyphicon-plus"></span>

		<?php echo __('Add slide', 'jigoshop-pro'); ?>
	</a>

	<ul class="list-group mt-2" id="slides">
		<?php 
		foreach($slides as $slideId => $slide) {
			Render::output('admin/theme_options/slider/slide', [
				'id' => $slideId,
				'slide' => $slide
			]);
		}
		?>
	</ul>
</div>