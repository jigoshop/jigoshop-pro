<?php
use ThemeOptions\Helper\Forms;
use ThemeOptions\Helper\Render;
?>

<li class="list-group-item">
	<span class="float-right">
		<a href="#" class="btn btn-danger slide-remove-button">
			<span class="glyphicon glyphicon-remove"></span>
		</a>
	</span>	

	<div class="clear"></div>

	<div class="col-sm-12">
		<?php 
		Forms::userDefined([
			'name' => 'image',
			'label' => __('Image', 'jigoshop-pro'),
			'display' => function($field) use ($id, $slide) {
				return Render::get('admin/theme_options/slider/image', [
					'id' => $id,
					'slide' => $slide
				]);
			}
		]);

		Forms::text([
			'name' => sprintf('slides[%s][text]', $id),
			'label' => __('Text', 'jigoshop-pro'),
			'value' => $slide['text']
		]);
		?>
	</div>
</li>