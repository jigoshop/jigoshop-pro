<a href="#" class="btn btn-primary slide-select-image-button">
	<?php echo __('Select image', 'jigoshop-pro'); ?>
</a>

<input type="hidden" class="slide-image-id" name="slides[<?php echo $id; ?>][imageId]" value="<?php echo $slide['imageId']; ?>" />

<span class="slide-image-name">
	<?php 
	if(isset($slide['imageId']) && $slide['imageId']) {
        $meta = wp_get_attachment_metadata($slide['imageId']);
        if($meta !== false) {
                echo basename($meta['file']);
        }		
	}
	?>
</span>