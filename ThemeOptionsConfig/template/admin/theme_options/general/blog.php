<li class="list-group-item" data-section-id="<?php echo $id; ?>">
	<input type="hidden" name="sections[<?php echo $id; ?>][type]" value="blog" />

	<span class="title"><?php echo __('Blog', 'jigoshop-pro'); ?></span>
	<span class="float-right">
		<a href="#" class="btn btn-danger section-remove-button">
			<span class="glyphicon glyphicon-remove"></span>
		</a>
	</span>
</li>