<?php
use ThemeOptions\Helper\Forms;
use ThemeOptions\Helper\Render;

Forms::select([
	'id' => 'section',
	'name' => 'section',
	'label' => __('Section', 'jigoshop-pro'),
	'options' => $sectionOptions
]);
?>

<div class="col-sm-12">
	<a class="btn btn-primary text-white" id="section-add-button">
		<span class="glyphicon glyphicon-plus"></span>

		<?php echo __('Add', 'jigoshop-pro'); ?>
	</a>
</div>

<div class="col-sm-12 mt-2">
	<ul class="list-group" id="sections">
		<?php 
		foreach($sections as $sectionId => $section) {
			Render::output(sprintf('admin/theme_options/general/%s', $section['type']), [
				'id' => $sectionId,
				'productOptions' => $productOptions,
				'productCategories' => $productCategories,
				'section' => $section				
			]);
		}
		?>
	</ul>
</div>