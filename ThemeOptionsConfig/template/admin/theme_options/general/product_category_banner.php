<?php 
use ThemeOptions\Helper\Forms;
?>

<li class="list-group-item" data-section-id="<?php echo $id; ?>">
	<input type="hidden" name="sections[<?php echo $id; ?>][type]" value="product_category_banner" />

	<span class="title"><?php echo __('Product category banner', 'jigoshop-pro'); ?></span>
	<span class="float-right">
		<a href="#" class="btn btn-danger section-remove-button">
			<span class="glyphicon glyphicon-remove"></span>
		</a>
	</span>

	<div class="clear"></div>

	<div class="col-sm-12 mt-2">
		<?php 
		Forms::select([
			'name' => sprintf('sections[%s][category]', $id),
			'label' => __('Category', 'jigoshop-pro'),
			'options' => $productCategories,
			'value' => $section['category']
		]);
		?>
	</div>
</li>