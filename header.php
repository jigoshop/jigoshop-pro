<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?php wp_title( '' ); ?></title>
	<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo( 'template_directory' );?>/style.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo( 'template_directory' );?>/blog.css" rel="stylesheet">
	<link href="<?php echo get_bloginfo( 'template_directory' );?>/assets/css/fontawesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

	<?php include( 'assets/js/JigoPro.php') ?> 
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head();?>
</head>
<header>
<div class="blog-masthead">
	<div class="js-header-container">
		<div class="js-header">
			<div class="js-header-content">
			<div class="js-row js-top-bar">
				<div class="js-row-container">

					<div class="js-column-container col-sm-6">
						<div class="js-column-inner">
						Welcome to our shop!
						</div>
					</div>
					<div class="js-right col-sm-6">Lang</div>
				</div>
			</div>
			<div class="js-inner-header">
				<div class="js-row-container">
					<div class="site-logo col-sm-2">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo esc_url( get_theme_mod( 'debut_logo' ) ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
					</div>
				</div>
			</div>	
			<div class="js-bottom-header">
				<div class="js-row-container">
					<nav class="blog-nav col-sm-9">
							<?php
wp_nav_menu( array( 
    'theme_location' => 'header-menu', 
    'container_class' => 'header-menu-class' ) ); 
?>
							
							<!-- <?php wp_list_pages( '&title_li=' ); ?> -->
					</nav>
				</div>
				<div class="js-mobile-menu">
					<div class="js-row-container">
					<nav class="topnav" id="myTopnav">
							<?php
wp_nav_menu( array( 
    'theme_location' => 'header-menu', 
    'container_class' => 'header-menu-class' ) ); 
?>
							
							<!-- <?php wp_list_pages( '&title_li=' ); ?> -->
						<a href="javascript:void(0);" style="" class="icon" onclick="myFunction()">&#9776;</a>	
					</nav>
				</div>
				</div>
			</div>

		</div>
		</div>
		</div>
</div>

</div>
</header>

<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
