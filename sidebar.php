<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
	<div class="sidebar-module sidebar-module-inset">
		<h4>About</h4>
		<p><?php the_author_meta( 'description' ); ?></p>
	</div>
	<div class="sidebar-module">
		<?php
		if( is_active_sidebar( 'blog-sidebar' ) ):
			dynamic_sidebar( 'blog-sidebar' );
		else:
			$the_widget_args = array(
				'before_widget'	=> '<div class="widget">',
				'after_widget'	=> '</div>',
				'before_title'	=> '<div class="widget-title"><h3>',
				'after_title'	=> '</h3></div>'
			);

			the_widget( 'WP_Widget_Categories', 'title=' . __( 'Categories', 'illdy' ), $the_widget_args );
			the_widget( 'WP_Widget_Archives', 'title=' . __( 'Archive', 'illdy' ), $the_widget_args );
		endif;
		?>
	</div>
</div><!-- /.blog-sidebar -->